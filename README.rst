PySchool
=========
A kit of lessons, programming exercises and support utilities used to create
workshops and labs to teach programming in Python.  The target environment is
PythonAnywhere.com andtheir education mode.

See (https://www.pythonanywhere.com/details/education).

The labs are not limited to this environment and can be applied elsewhere.

(TODO: Decide Appropriate Open License - CCx or BSD)

Why?
-----
There are some things that make these classes different from others available.

1. PySchool uses Python 3.  The labs and courses will focus on Python 3 and
   not    muddy the water by broaching the topic of compatibility. The target
   audience will not have the immediate need of learning the ins and outs of
   py2/py3 compatibility.

2. Students start writing code very quickly. Our goal is to explain, just
   enough, in a manner that doesn't inhibit learning the concept more
   completely later.

3. Example and lab code is all PEP 8 compliant, observing common conventions.
   This serves to teach by example and minimize bad habits that can develop
   from the student interacting with poorly written examples. This is not to
   say that 100 level students will be expected to write compliant code.

4. All code is written with an eye to testability.  This is enforced by every
   lab having associated tests already written.  These tests can be used by
   the teacher to ensure students have successfully completed a lab.  It also
   creates first contact memory patterns that will allow the student to learn
   to write test friendly code or approximations there of in a timely and
   appropriate way. Even in this    survey course, we are setting patterns of
   excellence before we even specifically address the topic.

5. The concept of Don't Repeat Yourself(DRY) is demonstrated as quickly as
   possible.  Code examples/labs that don't use DRY principals do the student
   a disservice.  Only when a lab is demonstrating the principals of DRY is
   repetitive code used and then only to show what to avoid.


Why PythonAnywhere?
--------------------
Reproducibility of an environment is helpful for programming and learning.
Installing Python on the student's operating system of choice is a worthy
topic, however, it can be a time consuming process. Removing obstacles not
germane to the target topics minimizes frustration for student and teacher and
can maximize time available for learning. PythonAnywhere(PA) provides a
solution to these challenges by providing free basic accounts on a remote
system that can be used for a learning environment. Giving access to that
remote system via Python Consoles, Bash Consoles and a decent editor with Idle
like qualities, all of which are web based and accessible via a browser. The
only requirement is Internet connectivity.

- Ensures that students and teachers have access to a Python interpreter that
  is installed and configured reliably.

- Provides a solution to the challenges of teaching students with
  heterogeneous local OSs.

- Allows the instructor access to the student's files.

- Allows the instructor to share their session with students.

- Allows the student to share their session with a teacher or other students.

- Students get to keep and access their work from anywhere and at anytime.

- Provides additional tools and software such as git, mercurial, vim, nano that
  may be used by student and teacher.

- Provides more advanced capabilities such as MySQL, web hosting and support
  for a number of Python web frameworks.

PySchool looks to build on this foundation by serving as a resource for
teachers and students.

Project Layout
---------------

- labs - a database of code challenges that can be used as the labs for courses.

- workshops - a database of courses

- ``sam`` - a utility to assist the teacher before, during and after the
  classroom.


Labs
----
A programming challenge/teaching aid for a specific topic. A lab consists of
primary components.

* [lab_name]

  * [lab_name].meta.json - Meta information in json format containing lab_name,
    target_audience, etc.  Catalog information for use by ``sam``.

  * [lab_name].py - the student lab file that will be modified by the student
    to complete the lab.

  * test_[lab_name].py - tests the students work to see if it passes. py.test
    is used to test the student's work. sam knows how to check the students work
    and report the results to the teacher.  An aid to know when students have
    completed the lab and an appropriate a tool for student to check their own
    work.  This test is written by the example author.

  * any additional source files necessary for completing the lab. i.e. data
    files, other python modules.

  * solution directory

    * [lab_name].py - a instructors copy with working source and appropriate
      notes.


Workshops
----------
These are the courses developed or in development. The naming structure is an
approximation of college/high school course names. i.e. py100 would be an
entry level course on Python, while py400 would be much more advanced topics.
Workshops that focused on a specific framework could be named django100, etc.

Workshops would contain the following materials:

- Lecture Notes - LECTURE.rst output to lecture.html via rst2html.py

- Slides - SLIDES.rst output to an S5 presentation, slides.html via rst2s5.py

- Labs - compiled from the available labs and dispersed and checked via the
  ``sam`` utility.

- Feedback/Survey - TODO


Better Examples and Labs
-------------------------
Many code examples currently available, unwittingly lead to suboptimal
learning experiences.  Our hypothesis is, "Students strongly internalize first
code contact." 

This makes those code examples key to getting them set off on the right foot.
Often examples set to teach a new keyword, construct or paradigm start off with
the simplest form possible.  However, many times the simplest form possible is
not the preferred/conventional way of doing something.  Most read up to the point
where they can accomplish the task at hand, which leads them to implement that
new idea in a suboptimal way.  Our goal is to present new ideas/concepts to
the students but in their most conventional or optimal form.  Teaching the
prerequisite knowledge needed to accomplish in the simplest, yet correct
manner.  

Quickly gettting the students to functions, as a unit of organization, is an
early goal.  Even before we get to more pythonic topics like lists and dictionaries.
For example, the following code is common in beginner programming courses all
over the web:

.. code:: python

    def say_hello(name):
        print('Hello, ' + name)

    say_hello('Sara')

The code above has a number of problems.  First, the code is problematic to
test since it mixes I/O with computation. This makes it much harder to test,
not impossible but harder than it should be.  It also has no docstrings at the
module or function level.

A better example would not mix I/O with computation. i.e.

.. code:: python

    """Example with testable code and docstrings."""

    def say_hello(name):
        """Create a personalized greeting string and return it."""
        return 'Hello, ' + name

    print(say_hello('Sara'))

Both examples output a string to the terminal, they do the same thing.
However, it is the way in which they get there, that makes all the difference.

Here it is quite easy to construct a test to see if the function ``say_hello``
does what is intended.  We also set an example of printing the return value of
the function.  Using the function as a proper function and not as a subroutine.

We need not even talk about testing to muddy the water, we just make sure to
always follow good conventions for writing testable code and when the student
is introduced to testing, they will have an innate ability to already write
testable code.

In addition, code that lends itself to testing, is also easier to refactor.
Instead of printing, say we want to return it as an web response, or let a
chatbot use it.  It also creates code that lends itself to be refactored from
a procedural style, to functional and object oriented paradigms. Testable code
has benefits beyond testing.

Also of note, docstrings are in the example. Written according to PEP 257.
While not focusing on docstrings specifically, we are creating an expectation
that they a norm, without discretely teaching it.

We believe that better examples and labs will maximize learning outcomes.
