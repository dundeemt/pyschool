.. include:: <s5defs.txt>
.. |sect|   unicode:: U+000A7 .. SECTION SIGN
   :rtrim:

py100 - Python for Beginners
==============================

.. class:: center

Welcome to py100

PythonAnywhere Account (|sect| 0)
----------------------------------
You will need a PythonAnywhere Account to complete the class labs. Follow
these instructions to setup an account.

- In your browser goto http://www.pythonanywhere.com/ 

- Click on **Pricing & signup**  -- don't worry, it's free

- Click "Create a Beginner account"

- Follow the instructions to create your account

Questions?  -- Just smile if you are done.

Introduction - py100  (|sect| 1) 
---------------------------------

- New to programming

- Interested in Learning Python

Introductions - Students (|sect| 1.1.0)
----------------------------------------
Even curiosity is a good reason to be here.

- Who

- Why

- What

Introductions - Teacher  (|sect| 1.1.1) 
----------------------------------------
- Who

- Why

- What

Introductions - TAs  (|sect| 1.1.2)
------------------------------------
- Who

- Why

- What

PythonAnywhere Account  (|sect| 1.2)
-------------------------------------
You will need a PythonAnywhere(PA) Account to complete the class labs. these instructions to setup an account.

- In your browser goto http://www.pythonanywhere.com/ 

- Click on **Pricing & signup**  -- don't worry, it's free

- Click "Create a Beginner account"

- Follow the instructions to create your account

Questions?  -- Just smile if you are done.


PA - Set Teacher  (|sect| 1.2.1)
-----------------------------------
You will need to tell the system that I am your teacher so we can access your
file system to set up the labs for you.

- From the **Dashboard**

- click **Account**

- click **Teacher**

- click **Enter your teacher's username**

- enter **dundeemt**

- click the :green:`Green Checkmark`

Once that is complete, go back to the **Dashboard** and wait.

PA - Dashboard (|sect| 2.0)
----------------------------
The dashboard is home base for PA.  If you get lost, go home.

- Tab: Consoles

- Tab: Files

Not used in this workshop

- Web
- Schedule
- Databases

Lab 1: Create student file (|sect| 2.4)
----------------------------------------
Create, edit and save a file.

- Navigate to your home directory
- Create a new file named **student**
- Enter your first name and last initial
- Save your work (hint: Ctrl-S is a shortcut.)

File Editor (|sect| 2.5)
-------------------------
- breadcrumbs
- Save  (Ctrl-S)
- Save As

Programming Languages (|sect| 3)
----------------------------------
.. image:: images/cpu.jpg
   :height: 384px
   :width: 365px
   :scale: 100 %
   :alt: alternate text
   :align: right

- Machine Language - ``11010100`` :  hard to read, registers, addresses, clock
- Assembler - ``JMP D8`` : easier to read, libraries, macros, same warts as ML
- FORTRAN - First commonly used High Level language. Comments, Variables
- LISP - Dynamically Typed / C - Systems / Pascal - p-code

Python - the language (|sect| 3)
-----------------------------------
- "Batteries Included" 
- Imperative, OO, Functional, Dynamic
- Meritocracy - Freedom and Conventions
- Readable

What is a program? (|sect| 3.0.1)
-----------------------------------
A program is a series of computer instructions intended to give a desired
result.  The program, in general, has three(3) primary actions:

- **input**: Get the data to process.

- **process**: Do something with or to that data.

- **output**: Return the result of the operation


The Python Interpreter (|sect| 3.1)
--------------------------------------
- From your Dashboard, Start a new console, click on ``py3.4``

.. code:: pycon

    Python 3.4.3 (default, Oct 14 2015, 20:28:29)
    [GCC 4.8.4] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> 

- Type ``2 + 3`` and press <Enter>
- Type ``5 - 3`` and press <Enter>
- Type ``print("Hello World!")`` and press <Enter>

*Note: double quotes (") around "Hello World!" they are important*

The Python Interpreter. (|sect| 3.1)
--------------------------------------
- From your Dashboard, Start a new console, click on ``py3.4``

.. code:: pycon

    Python 3.4.3 (default, Oct 14 2015, 20:28:29)
    [GCC 4.8.4] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> 2 + 3
    5
    >>> 5 - 2
    2
    >>> print("Hello World!")
    Hello World!

What just happened? (|sect| 3.1)
-----------------------------------------
.. code:: pycon

    >>> print("Hello World!")
    Hello World!
    >>> 

- **built-in** - functionality available at all times

- **function** - A function is a block of organized, reusable code that is
  used to perform a single, related action.

- **string** - An immutable sequence of characters.

- **literal** - A value that represents itself exactly. 


Python Console - REPL (|sect| 3.1)
------------------------------------------
When you enter some code and press <Enter>

- Read
- Eval
- Print
- Loop

This is an example of Python using the best ideas available. REPL gives you
the ability to quickly "try things out" and experiment. This is a powerful
capability.  Thanks LISP!


Data Types (|sect| 4.0)
------------------------------------------
A data type defines what the data represents. 7 vs. "7"

- It determines the possible values for that type.
- Operations that can be done on values of that type.
- How values of that type can be stored.

.. code:: pycon

    >>> print(7)
    7
    >>> print("7")
    7

(cont.)

Data Types - ``type`` (|sect| 4.0)
------------------------------------------
Lets use the Python function ``type`` to see the difference between "7" and 7

.. code:: pycon

    >>> type("7")
    <class 'str'>
    >>> type(7)
    <class 'int'>

Python refers to strings as **str** and integers as **int**

How do Types help me? ( |sect| 4.1)
------------------------------------------

