==========
py100
==========
**target audience** - new to programming / never programmed

Pre-Class
=========
Things to be accomplished before class start

- Setup projectors

- Setup instructor laptop

- Display the Getting a basic/free account set up on PythonAnywhere.com (slide1)

  - Link to Slides_ - would be nice to have an official pyschool slide
    template.

- Instructor encourages students to get setup before class begins, TAs
  roam and provide assistance as needed.

.. _Slides: slides.html

Section 1 - Intro and Setup
===========================
Introduction to Python and pythonanywhere environment that they will be using.

1.1: Introductions
-------------------
- Who is the classed aimed at and the goals of the class

- Student Introductions

- Instructor Introduction

- Introduce Assistants

1.2: Setup
----------
This is the same information/activity done during the Pre-Class

- Setup a Basic account on PythonAnywhere

- Set your teacher

- ``sam roster``  should display the same number of students as you have in class

Section 2 - PythonAnywhere Web Interface
========================================
Introduce and familiarize everyone with the PythonAnywhere interface:
Dashboard, consoles, files.

Go to http://PythonAnywhere.com/ and log in, if you are not already and you
will see the Dashboard.

2.1: Dashboard
---------------

2.2: Consoles tab
------------------

- Python - version 3.4 -> 2.6.  Clicking on 3.4 starts an interactive
  Python shell.  ``2 + 2<Enter>``, click back on Dashboard

  - ipython - a souped-up version of the python shell.

  - pypy - Python written in Python. (Explain the different implementations of
    Python. CPython, PyPy, Jython)

- Bash     (tip: /frame)

  - MySQL - a database shell

  - Custom - we won't be using

- Your Consoles - explain how the consoles/shells will continue to run
  until explicitly closed.  Can come back and re-attach from a different
  machine, closing your browser, restarting your machine.

  - Find the one marked *python3.4 conole* and click on it.

  - Ctrl-D or ``exit()`` to close the shell

  - Back to Dashboard

- Consoles Shared with you - how consoles can be shared and what sharing
  means.

2.3: The Files tab
-------------------
Demo how to navigate around user file system and the different parts of the
interface.

- navigation

  - breadcrumb

  - directories

  - files

The following tabs are available, but are not used in this class.

- Web
- Schedule
- Databases


2.4: Lab - Create student file
-------------------------------
Practical example to create, edit and save a file.

- Go to your home directory and create a new file named 'student'

- Now open the file for editing.  Edit is the default action if you click on
  the filename.  The icons to the right of the name are:
  - Download
  - Edit
  - Delete

- Edit the file
  - In the student file, type your first name and first initial of last name.

  - unsaved changes indicator

    - keyboard shortcuts

      - Ctrl-S : Save

- ``sam roster`` - should now show student's names.

2.5: File Editor
-----------------

- breadcrumbs
- shortcuts
- Save
- Save As
- **>>>** - Save and Run. **Not present when the file is not runnable.**  Need
  to point this out when we edit our first python file.


Section 3: Programming Languages
=================================
At the heart of all *computers* is a Central Processing Unit (CPU). The CPU
takes in signals on a physical connection. A group of these connections is
called a bus.  Each connection represents a bit (1 or 0 - on/off). An 8bit
computer has 8 lines in its bus, a 64bit computer has 64 lines in its bus.

To interact with the CPU, you need to set the bus to a desired state and
then signal the CPU to read the bus. When it is done, the CPU sets the new
state of the bus and signals the outside to read it.  This setting of the bus
is done in Machine Language.  It makes it possible to communicate with the
CPU but it is quite difficult for a human to use in a productive manner. This
is considered a Low Level language.  Over time, higher level languages have
been created to make it easier for humans to interact with computers.

Assembler - Uses mnemonics and hexadecimal to make Machine Language easier to
read.  ''MOV #100,R0'' is an example statement. It is MOVing the value 100
into Register 0. Like ML, assembler is processor specific. Different
processors have various numbers of registers, addressing schemes, and
supported commands.

FORTRAN - FORmula TRANslation system, NOT the first high level language but
the first widely used high level language. Allowed a program to be written that
could, with only minor modifications, run on different processors. This was
achieved by compiling the code to machine language. The compiler was specific
to the computer architecture. The compiler handles registers, addresses, etc.

LISP - a dynamic functional language developed a year after FORTRAN and still
in use today. Read-Eval-Print-Loop (REPL)

C - Used for building Operating Systems because it can used where you
previously used assembler, memory management, widely used.  Pointers,
out-of-bounds, memory corruption, incredibly easy to shoot one's foot but very
powerful.

Pascal - Safer than C, encouraged structured code. p-Code. Byte Compiling to a
virtual Pascal machine.  Only the p-machine needed to be ported to other
computer architectures.

These higher level languages are designed to make it easier for humans to
not only interact with the computer but also make it easier to write bug
free code that operates as the programmer intended. I single out these
previous language because Computer Languages do not appear from the void, they
build on the work and knowledge gained from predecessors.

(Next Slide - "Python - the language")
Python is a high level language that allows a programmer to write
code that is easier to read and understand than many previous languages.
It allows the programmer to do more with less code, and able to understand
the intent of other code, not written by that programmer, which makes for
increased productivity.

see: https://www.python.org/doc/essays/blurb/

3.0.1: What is a program?
--------------------------
A program is a series of computer instructions intended to give a desired
result.  The program, in general, has three(3) primary actions:

- input: get the data to process.

- process: do something with or to that data.

- output: return the result of the operation


3.1: The Python Interpreter
----------------------------
The Python interpreter has two primary roles, to run programs written in
Python and it also provides an interactive shell.

- From your Dashboard, Start a new console, click on ``py3.4``

- you should see the Python prompt.

.. code:: pycon

    Python 3.4.3 (default, Oct 14 2015, 20:28:29)
    [GCC 4.8.4] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> 

- Type ``2 + 3`` and press <Enter>

- The Python interpreter/shell responds with the output ``5``

- Type ``5 - 3`` and press <Enter>

- You will see, ``2`` as the response.

- Type ``print("Hello World!")`` and press <Enter>
  *Note the single quotes (') around 'Hello World!' they are important*

.. code:: pycon

    >>> print("Hello World!")

- you should see:

.. code:: pycon

    >>> print('Hello World!')
    Hello World!
    >>> 

- What just happened? We used one of Python's builtin functions, ``print`` to
  print the string literal, 'Hello World!' to the screen.
  See: https://docs.python.org/3/library/functions.html#print

  - **function** - A function is a block of organized, reusable code that is
    used to perform a single, related action. Functions create modularity
    which is good for code reuse.

  - **string** -  is a sequence of characters. Strings are immutable objects.
    This means that once defined, they cannot be changed.

  - **literal** - an object that is the literal definition of itself. (i.e.
    'Hello World!' is a string literal and the number 77 is a literal integer.

  - A string has a type, ``str``.  Python is strongly typed. Which means all
    things in Python have a type associated with them and Python will only
    allow you to do things that make sense with a given type.


Section 4 - Types
=================
What are types?  A type defines what the data represents.  For instance, the
string literal "7" is a different thing than the integer 7.  However, when you
print the string "7" or the integer 7 the output looks exactly the same.


.. code:: pycon

    >>> print(7)
    7
    >>> print("7")
    7

Lets use the Python function ``type`` to see the difference between "7" and 7

.. code:: pycon

    >>> type("7")
    <class 'str'>
    >>> type(7)
    <class 'int'>

Python refers to strings as **str** and integers as **int**

4.1: So what do types really do?
---------------------------------
From before, we are confident that if we enter ``7 + 7`` at the prompt and
press <Enter>, Python will respond with ``14``.  So, based on that, how do you
think Python will respond if you were to enter ``"7" + "7"``, go ahead and try
it, you might be surprised, but in the end - I guarantee it will not be
surprising.

.. code:: pycon

    >>> "7" + "7"
    '77'

In Python, strings support the addition operator ``+`` but the operation is not
mathematical addition, it is concatenation, Or combining two strings to create
a new string object.  *Remember: Strings are an immutable type.*

4.2: Types protect you
-----------------------
Types also protect against doing things that are undefined. For instance,
trying to add the string "7" to the integer 7.  What do you think will happen?
Would you get the integer 14, or the string "77"?  Let's try it out.

.. code:: python

    >>> "7" + 7
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    TypeError: Can't convert 'int' object to str implicitly

Python raises an Exception.  From the error message, you can see that Python
doesn't know what you want.  You need to be explicit about what you want to
happen.  Either you can cast the string "7" to an integer and get 14 or you
can cast the integer 7 to a string and get "77".  i.e.

.. code:: pycon

    >>> int("7") + 7
    14
    >>> "7" + str(7)
    '77'

To cast, is to convert an object into another type of object.

4.3: Strings - Single Quote(') or Double Quote(")?
--------------------------------------------------
Strings can be enclosed by matching single quotes or matching double quotes.
When to use which is up to the programmer. The one issue of note is that if
there are embedded quotes within the string, you can use the alternate quotes
to make the string without having to escape the quotes within the string.
Examples using alternate quotes:

.. code:: pycon


    >>> "Do you have Sam's ball?"
    >>> 'Have you read, "Forging Zero", by Sara King?'


Examples escaping same quotes:

.. code:: pycon


    >>> 'Do you have Sam\'s ball?'
    >>> "Have you read, \"Forging Zero\", by Sara King?"


Or something with both:

.. code:: pycon


    >>> "Have you read, \"Ender's Game\", by Orson Scott Card?"

There is only one exception to this rule about use whatever quote type you want
and that is for docstrings (Which we have not talked about yet.) Docstrings
should use double quotes by convention.

4.4: Strings - Concatenation
-----------------------------
String concatenation is a very useful bit in everyday programming. Using our
"Hello, World!" example.  Let's break it out into a greeting, who and
punctuation.

.. code:: pycon


    >>> print('Hello' + ',' + ' ' + 'World' + '.')

4.5: Comments (#)
---------------------------
The octothorpe (a/k/a hash mark, pound symbol, tic-tac-toe) is used to denote a
comment in a Python.  Comments in Python start with the hash character
(#) and extend to the end of the physical line. A comment may appear at the
start of a line or following whitespace or code, but not within a string
literal. A hash character within a string literal is just a hash character.

.. code:: pycon

    >>> # This is a comment.
    ... 
    >>> 2 + 2 # should respond with 4
    4
    >>> 
    >>> print("This is just a hash mark -># and doesn't affect the output.")
    This is just a hash mark -># and doesn't affect the output.
    >>> 


4.6: Lab - helloworld
----------------------
Let's write some code.

- Go to you **Files** tab and navigate to: pyschool/py100/helloworld

- Open the module, helloworld.py for editing, by clicking on it.

- On the line after the comment (#) write the code to print out 'Hello, World!'

- Click on the "Save and Run" button ``>>>``, top right button.  A new window
  should open, if it doesn't make sure you have your pop-up blocker disabled
  for www.pythonanywhere.com

- Your code will run and you will be left at a Python prompt, >>>.  You should
  see:

.. code:: pycon

    Hello, World!
    >>>

- If the code runs, but you see a mistake, close the pop-up window, make the
  corrections and re-click the "Save and Run" button.  Repeat this until you
  have the intended results.  If you need assistance, just raise your hand and
  one of the TAs will come over to help.  If you have a general question, just
  go ahead and ask.

- Since this is the first code run for everyone in the room, it might take a
  few minutes to get everyone complete.  So if you finish and don't have
  questions please feel free to stretch you legs, use the facilities.  We'll
  start back up in 5 minutes.

4.7: String Interpolation
--------------------------
While you can do most everything you want with string concatentation, it can
get to a point where the string becomes hard to read.  See Concatentation
example.  There are two other builtin ways to handle situations where
concatenation has become burdensome.  They are the string modulo operator and
the .format method.

4.8: String - modulo (%)
------------------------
The modulo operator, ``%`` (a percent symbol) can be used as a marker for
substituting text. 

.. code:: pycon

    >>> print("Hello, %s." % 'World')
    Hello, World!

Here we use ``%s`` to indicate that we are going to be substituting a (s)tring
for the marker.  Then we have the ``%`` to indicate what comes next are the
substitution value for the marker in the string. And then finally, what to use
for that value.  If we wanted to substitute more than one thing in the string,
then we would have a marker for each and a value for each.

.. code:: pycon


    >>> print("%s, %s." % ('Hello', World'))
    Hello, World!
    >>> print("%s, %s." % ('Goodbye', 'Cruel World'))
    Goodbye, Cruel World!

**Note**: If we have more than one value to substitute, then we enclose those
values after the module symbol with open and close parenthesis, ().

Also, the number of markers and the number of values have to match or Python
will raise an Exception.  Which is helpful, because what you have written is
most likely not what you intended.

The markers can be explicit as to what type of data will be used to replace it.
You can indicate that the replacement will be an integer by using ``%i``.

.. code:: pycon


    >>> print("%i %s in a %s." % (7, 'days', 'week'))
    7 days in a week.

However, if you use that same string template and try to pass the string "7" as
the first substitution value, Python will raise an exception because the type
expected does not match the type given.

.. code:: python


    >>> print("%i %s in a %s." % ('7', 'days', 'week'))
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    TypeError: %i format: a number is required, not str

Quite frequently using a %s will do what you want.

.. code:: pycon


    >>> print("%s %s in a %s." % (7, 'days', 'week'))
    7 days in a week.

The above works, because Python has a string representation for everything.
The reason the former worked and the latter didn't is that while Python has a
string representation for everything, not everything can be cast to an integer.


4.9: Lab - helloworld (redux)
-----------------------------
Now that you know about string interpolation with the modulo operator let's
refactor helloworld.py to use interpolation for the name part of Hello, World!

- Open pyschool/py100/helloworld/helloworld.py

- edit the line ``print('Hello, World!')`` and replace ``World`` with an
  interpolation marker, then add the modulo operator and the value to
  substitute.

- to refactor, is to modify existing code to make it more readable, more useful
  or to make the code less brittle.  Brittleness is when a code change else
  where causes this code to break.  The opposite of brittle is robust, which
  means more adaptable and able to accept a wider range of values but still be
  able to accomplish what the programmer intended. Refactoring does not change
  functionality.

- again, if you have any questions, please ask.  If you are having technical
  problems please signal a TA.

.format - is another form of string interpolation but we will not be covering
it in this class.

Section 5 - Variables, Identifiers & Assignment
===============================================
Everything we have done so far, has used literals for the data. Recall that a
literal means exactly what it says, like 'hello' or 7.  A variable, in
contrast, can have a value that is not directly related to its name. Variables
identify a particlar datum or value.  i.e.

.. code:: pycon


    >>> hello = 'Hello, World!'
    >>> print(hello)
    Hello, World!
    >>>
    >>> hello = 'Hello, Sam.'
    >>> print(hello)
    Hello, Sam.

You will note that the equals sign is used, but it does not mean the same thing
as it does in math.  It is an assignment statement. You are assigning the
identifier ``hello`` to the string literal, ``'Hello, World!'``.

Variables can be assigned, not just to literals, but other identifiers,
expressions, functions and objects.

Lets try out variables.

.. code:: pycon

    >>> height = 8
    >>> width = 10
    >>> height * width
    80
    >>> print("%i times %i equals %i" % (height, width, height * width))
    8 times 10 equals 80

In math class, you used a symbol that looks like an x to signify
multiplication, in most computer languages the symbol used to denote
multiplication is the asterisk (*).

The sequence of characters used to form a variable name (and names for other
Python entities later) is called an identifier. It identifies a Python variable
or other entity.

see: https://docs.python.org/3.5/reference/lexical_analysis.html#identifiers


5.1: Identifier Rules
-----------------------
The are some specific rules for creating identifiers:

- may include a-z, A-Z, 0-9 and underscores (_)

- may not start with a number

- length is unlimited

- may not be a Python keyword. (listed below)

        True                elif                in                  try
        and                 else                is                  while
        as                  except              lambda              with
        assert              finally             nonlocal            yield
        break               for                 not
        class               from                or
        continue            global              pass

- Identifiers are case sensitive. BLUE, Blue, bLue, blue are all different
  identifiers, however please read the next section on conventions before
  going crazy.

There are also some standard conventions for creating identifiers:

- Use all lower case

- seperate words with underscores(_). i.e. ``sphere_volume``

- Only use camelCase if working on existing body of code that already uses
  them.

5.2: Help - where did you get those keywords
---------------------------------------------
Did they come from a book? Nope, you've got them at your fingertips right now.
Remeber when you first started a Python shell and it said:

.. code:: pycon

    Python 3.4.3 (default, Oct 14 2015, 20:28:29)
    [GCC 4.8.4] on linux
    Type "help", "copyright", "credits" or "license" for more information.

See the first two words on the third line?  Go ahead and type ``help`` <Enter>

.. code:: pycon

    >>> help
    Type help() for interactive help, or help(object) for help about object.

So go ahead and enter help()

.. code:: pycon

    >>> help()

    Welcome to Python 3.4's help utility!
    If this is your first time using Python, you should definitely check out
    the tutorial on the Internet at http://docs.python.org/3.4/tutorial/.
    Enter the name of any module, keyword, or topic to get help on writing
    Python programs and using Python modules.  To quit this help utility and
    return to the interpreter, just type "quit".
    To get a list of available modules, keywords, symbols, or topics, type
    "modules", "keywords", "symbols", or "topics".  Each module also comes
    with a one-line summary of what it does; to list the modules whose name
    or summary contain a given string such as "spam", type "modules spam".
    help> 


Second paragraph, "To get a list of ... keywords, ... type ... "keywords", so
let's try it.

.. code:: pycon

    help> keywords

    Here is a list of the Python keywords.  Enter any keyword to get more help.
    False               def                 if                  raise
    None                del                 import              return
    True                elif                in                  try
    and                 else                is                  while
    as                  except              lambda              with
    assert              finally             nonlocal            yield
    break               for                 not
    class               from                or
    continue            global              pass
    help> 

Python's help system is quite helpful, complete and always at your fingertips.
We'll talk more about the help system in an upcoming section.


Section 6 - Functions
=====================
A function is a block of organized, reusable code that is used to perform a
single, related action.

- Functions create modularity which is good for code reuse.

- Functions return a value.

- The return value maybe ignored. (i.e. ``print``)

- Functions are first-class objects. Specifically, this means Python supports
  passing functions as arguments to other functions, returning them as the
  values from other functions, and assigning them to variables or storing them
  in data structures.

Let's look at
two simple functions:

.. code:: pycon

    >>> def two():
            """always return the integer 2."""
            return 2
    
    >>> def add_two(number):
            """add 2 to number and return result."""
            return number + 2
    
    >>>


The first function, ``two`` takes no arguments (a/k/a parameters, args) and
simply returns the integer 2 when called.

.. code:: pycon

    >>> two()
    2

The second function, ``add_two``, requires one argument. It then attempts to
add 2 to that argument and then returns the result.

.. code:: python

    >>> add_two(3)
    5
    >>> add_two(6)
    8
    >>> add_two()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: add_two() missing 1 required positional argument: 'number'

Calling the function, add_two without an argument resulted in an Exception,
where Python is telling us, that it is missing the argument for 'number'.

To make your function calls self-documenting, you can and should reference the
arguments by name, if it makes the code more clear.

.. code:: pycon

    >>> add_two(number=3)
    5

A function can be used in an expression. Here the function add_two is
evaluated first, with an argument of 3.  It returns 5.  2 is then added to 5.

.. code:: pycon

    >>> 2 + add_two(number=3)
    7

A function name is an identifier and follows the same rules as listed in the
Identifiers section before.  To create a function:

- you start with the keyword, **def** which is short for define. -- **def**

- That is followed by the function's identifier -- **def area**

- then an open parenthesis. **(** -- **def area(**

- Then 0 or more arguments, in our example there are two. -- **def area(
  height, width**

- then a closing parenthesis **)**  -- **def area(height, width)**

- and finally, a colon **:**  -- **def area(height, width):**

- The body of the function is indented from the column were 'def' is located.

  - Normal indentation is 4 spaces.

  - Do not use TABS.

  - You can use your tab key if you set your TAB key to insert spaces instead
    of hard Tabs.

- The first line of the function body should be a docstring.

.. code:: pycon

    >>> def area(height, width):
            """Return the area of the rectangle."""
            return height * width

    >>>help(area)
    Help on function area in module __main__:
    area(height, width)
        Return the area of the rectangle.
    (END)

If a function does not explicitly return, then Python issues a ``return None``
at the end of the function body.  If the function explicitly calls ``return``
but does not specify a return value, Python sets the return value to ``None``.


6.1: Datatypes: None
--------------------
So far we have dealt with str, int, and function data types.  Python has a
particular data type, None.  The sole value of the type NoneType. None is
frequently used to represent the absence of a value, as when default arguments
are not passed to a function. Assignments to None are illegal and raise a
SyntaxError.


6.1: Lab: hello_sam
--------------------
Now that you have learned a bit about using and writing your own functions,
lets create a variation of helloworld.py that is a bit more robust.

- open pyschool/py100/hello_sam/hello_sam.py

- using either concatenation or interpolation, return a string that says,
  "Hello, Tim." if 'Tim' is passed in as the name argument.

- Also, note that unlike the function that complained about a missing argument,
  the hello function doesn't, it just uses the string 'Sam'.

6.1.1: Code Review
-------------------
The argument list consisted of an identifier being assigned a value.  Yet, if
you passed an argument in, it completely ignored the assignment, if you passed
in no value, it used the assignment and didn't throw an exception about a
missing argument.  This is called a default argument.  It is used when an
argument is missing and a default value is provided.  There are a couple of
caveats to using default arguments but they will be covered in a later class.

6.2: More fun with functions
------------------------------
The return value of a function can be assigned to a variable and by extension
used as an argument to another function.

 TODO - show assigning a variable to the result of a function. Then show using
 a function return value as an argument to another function.

Since Python has first class functions, you can assign a function to a
variable and by extension as an argument to another function.  

 TODO - Example passing a function as an argument. Start by showing assignment
 of a function to a variable.


Section 7: Booleans and Conditionals 
=====================================
So far our code and examples just do and never decide anything.  A conditional
is a simple construct, if this then that.

.. code:: pycon

    >>> condition = True
    >>> if condition:
            print('condition was True')
    
    condition was True
    >>> 
    >>> condition = False
    >>> if condition:
            print('condition was False')
    
    >>> 

7.1: Boolean
------------
In the above example we used a new datatype, Boolean.  The boolean datatype has
two values: ``True`` and ``False``.  These are also reserved words in Python.

7.2: if
--------
``if`` is a Python keyword and has the form:

.. code:: pycon

    if condition:
        conditional_code_block

Note how the code to execute if the condition evaluates to True, is indented
from the ``if`` statement.  Indentation is a key concept in Python.  It is a
method to group code.

.. code:: pycon

    >>> def ifcon(condition):
    ...     """demo if statement"""
    ...     if condition:
    ...         print('in conditional code')
    ...     print('uncoditional code')
    ... 
    >>> ifcon(True)
    in conditional code
    unconditional code
    >>> ifcon(False)
    unconditional code

7.3: comparisons
----------------
There are six(6) comparison operators that can be used in an ``if`` statement.
Here are a few, please try them out in your python console.

.. code:: pycon

    >>> x = 5   # not a comparison but an assignemnt
    >>> x == 5
    >>> x < 3
    >>> x > 3
    >>> 2 * x > 10
    >>> 10 < 2 * x + 1
    >>> x < 2 * x
    >>> x >= 5
    >>> x <= 5
    >>> x != 5

The first, which is an assignment statement, set x = 5.
Did you notice the 2 equals signs? While ``=`` is an assignment statement,
``==`` is an equivalency comparison.
The next compares the value of variable ``x`` to the literal 3.

7.4: comparisons continued
--------------------------
You can compare more than just integers, as long as Python can make sense of
the comparison.

.. code:: pycon

    >>> 'a' < 'b'
    True
    >>> 5 == 5.0
    True
    >>> type(5.0)
    <class 'float'>

7.4.1: datatype float
---------------------
Here is a new Python datatype, ``float``. For now, it is enough to know that
they can represent numbers with decimal places. i.e. ``0.825``

7.5: Lab - organize_sam.py
--------------------------
Sam just discovered that his room is disorganized and he wants to straighten
things out.  However, Sam's Neural Nets are new and need training.  Can you
help out Sam and complete his organize function?

- Open pyschool/py100/labs/organize_sam/organize_sam.py

- Finish writing the organize function so that it returns a string stating
  the correct order of the two items Sam supplies.

- There is a function, ``test()`` that requires no modification.  That function
  runs if that module ``organize_sam.py`` is run from the command line or from
  your Run & Save button.

- The funky looking ``if __name__`` statement at the bottom, handle the logic
  for detecting if the module is being run or if it is being imported by
  another module. (more on importing modules later.)

7.5.1: code review
------------------

- How many if statements do you have?

- How many return statements?

- When writing if statements, it is desirable to avoid negative logic. It makes
  debugging and comprehension easier.  This is a rule of thumb not a hard rule.
  For instances, "Don’t ever not avoid negative logic" vs "Avoid negative
  logic".

7.6: if - else
---------------
In the previous example, you used simple if statements. Those simple ifs can do
anything, the problem is that it can take quite a few and make your code more
difficult to write and read.  There are two other parts available to an if
construct.  The first is ``else``.

.. code:: pycon

    if condition1:
        do_this()
    else:
        do_that()
    always_do()

The if-else creates a fork in the logic, doing either do_this() or do_that()
and then the logic joins up again afterwards to run the always_do().  We could
have written this with just simple ``if`` statements but it is harder to read.

.. code:: pycon

    if condition1:
        do_this()
    if not conditon1:
        do_that()
    always_do()

The condition1 logic is executed twice (wasteful) and reading through the code
and understanding it's intent is marginalized.  It takes more effort to
understand.

7.6.1: if - elif - else
-----------------------
What if you have more than one conditions? ``elif`` allows for this use case.
Say you had to write a function to convert a numeric grade to a letter grade.

.. code:: python
   :number-lines:

    def convert_to_letter(grade):
        """Convert the numeric grade to a letter grade using Hogwart's 7 point
        scale."""
        if grade >= 93:
            letter = 'A'
        elif grade >= 86:
            letter = 'B'
        elif grade >= 79:
            letter = 'C'
        elif grade >= 72:
            letter = 'D'
        else:
            letter = 'F'
        return letter


If the numeric grade is 92, then the first conditional fails, but it passes
the second, sets the letter to 'B' and jumps over the rest of the if-elif-else
structure and returns the value of letter.

The conditon evaluations start at the top, each if/elif is evaluated in order
until one of them evaluates to ``True``.  When it evaluates to ``True``, the
associated code block is executed and then program flow jumps to the end of
the ``if`` structure and continues.  If none of the conditional expressions
evaluates to ``True``, then the ``else`` block is executed, if it exists.

You get one ``if`` and one ``else`` but you can have 0 to N ``elif``
statements between them.

7.7: Lab - organize_sam_redux.py
--------------------------------

- go to pyschool/labs/organize_sam/organize_sam_redux.py

- rewrite your code from simple if statements in organize_sam.py to use an
  if-elif-else structure.

- test your code out by executing it and organize some more values.


7.7.1: Code Review
------------------

- compare and contrast organize_sam.py and organize_sam_redux.py:

  - readability

  - usability


7.8: the almighty if
---------------------
``if`` and it associated jump, is the king of programming constructs. Most of
the flow-control constructs you will learn can be recreated by simply using
``if`` statements.  However, it will take more code to accomplish -- this is
the reason that the other flow-control constructs exist.  In programming, it
can be said that "Laziness is a constructive attribute."

7.9: Lab - Hogwart's backwards
--------------------------------
Because the if statement is powerful, it is important that you get some
experience thinking about how to use it.

- go to pyschool/labs/hogwarts/hogwarts.py

- complete the function ``grade_to_letter_backwards``, in which you should
  evaluate if the grade is an 'F', then 'D', ..., all the way to the else
  clause which will return 'A'.

- the original function is already written and is there for you to test your
  new code against.

7.9.1: Code Review
-------------------

- What happens if you evaluate a grade of 92.99 ?

- How did the comparison operators change when you wrote the function to
  assign a letter grade checking for an 'F' first as compared to checking for
  an 'A' first?


Section 8 - Modules
========================
The upside of using the Python interactively, is the speed at which you can try
out code and do experimental coding when working with new ideas or refreshing
old knowledge.  The downside, is when you quit the interpreter your work is
gone.  If you wish to save your code and be able to run it again later is to
record your Python code in a file.  These types of files are sometimes called
source code files the proper Python term is a module.

8.1: naming
------------
Modules should have short, all-lowercase names. Underscores can be used in the
module name if it improves readability. The file extension is .py. i.e.
``helloworld.py`` or ``hello_world.py``.

8.2: Layout
-------------------
Python does not require that a module have a specific layout. It only needs to
be legal Python code.  There are conventions though, that make your module
easier to use and resuse by others and even your future-self.  Here is a
conventional layout for a Python module. It is named: ``myapp.py``.

.. code:: python
   :number-lines:

    #!/usr/bin/env python3
    """myapp - program to do something I wanted to automate."""

    # other code goes here, get_input, process, output and other functions

    def main():
        """The original purpose of this module."""
        data = get_input()      # get data from the user or source
        result = process(data)  # do something to or with that data
        output(result)          # show user or store results

    if __name__ == '__main__':
        main()

8.2.1: shebang line
--------------------
Line 1 is commonly known as a shebang line.  It is an operating system(OS)
convention that specifies what interpreter to use.  It is common across a wide
range of shell scripting and dynamic languages.  When the OS executes an
application, part of the process is inspecting the code to be run. In this
case, we are informing the OS that we want to use Python3.  Many systems have
both python2 and python3 available and if you don't specify which, the OS will
pick for you.  Since there are differences between Python2(py2) and
Python3(py3) we want to ensure that the module is executed with the approriate
version of Python. It is written as a comment.

8.2.2: docstring
-----------------
Line 2 is a docstring is placed at the top of the module, so that it is the
first Python statement after any comments. The docstring is a
triple double quoted string, and lists what the module does.  It does not
affect the functionality of the module but serves as documentation and is a
standard by convention. Docstrings can be accessed by the __doc__ attribute
on objects.  If your docstring is a one-liner that it would look like this:

.. code:: python


    """helloworld - prints out, Hello World!"""

a docstring that is longer than one line would look like this:

.. code:: python

    """helloworld - prints out, Hello World!
    It is a part of the pyschool, py100 class labs."""

see: https://www.python.org/dev/peps/pep-0257/

8.2.3: if __name__ == '__main__':
----------------------------------
Line 12 - One of Python's tennants is code reusability.  Often times you will
find yourself wanting to use a function or other code you or someone else has
already written.  Python makes this incredibly easy with its ``import``
mechanism.  However, importing code is to execute the imported code in the
current module's context.  This means that if the code you are importing just
does its thing, that same thing will happen when you import it.  This almost
always is not what you intend when you are reusing code. You want the imported
code to behave itself and only do what you intend.

Python has a module attribute, ``__name__`` that is set to ``__main__`` if
this module was directly invoked.
see: https://docs.python.org/3/library/__main__.html

Given this partial Python program, we see the ``if __name__ == '__main__'``
line.  If myapp.py is invoked directly then the statement is True and function
main() is automatically run.  If we want to use the process() function or other
supporting functions in a different app and supply input and/or do something
something different with the result of process(), we just want access to those
functions and do not want the imported code to automatically run the
get_input() and output() functions.  In that case, when that module is imported
into a different app, that modules __name__ is not set to __main__, so the if
statement is False and the main() function is not automatically called.

The function name in the if statement does not have to be called, main(), it
could be run_tests() or whatever you choose.


Section 9 - Getting Input
============================
Earlier we said that a computer program is composed of:

- input: get the data to process.

- process: do something with or to that data.

- output: return the result of the operation

We have talked about one way to output the result, ``print``. We've looked at
some ways to manipulate the data, adding, subtracting, concatenation and
interpolation.  However, all the code until now, has had input data as a
part of the source code.  Here we are going to look at one way we can get input
from a user.

Python's ``input1`` function can receive keyboard input from a user on the
console.

.. code:: pycon

  >>> input("Enter a number: ")
  Enter a number: 27
  '27'
  >>> 


When you run this code in your console, Python prints out the string and then
waits for the user to type something and press <Enter>.  If you haven't
already please try it for yourself.

9.1 - Storing their input for later
------------------------------------
Note that the response is always a string, even if the user entered 27.
Try this:

.. code:: pycon

  >>> answer = input("Enter a number: ")
  Enter a number: 27
  >>> answer
  '27'
  >>> type(answer)
  <class 'str'>
  >>> 

9.2 - Getting a number
-----------------------
If you need the input to be a proper integer, you will need to cast the
result to an integer like this:

.. code:: pycon

  >>> answer = int(answer)
  >>> type(answer)
  <class 'int'>
  >>> answer
  27

Lab 9.4: padder.py
--------------------------

- go to pyschool/labs/padder/padder.py

- 

- test your code out by executing it and organize some more values.


Section 10 - Loops
====================
In general, a loop is a group of instructions that are continually repeated.
If there is no way to stop looping, it is referred to as an Infinite Loop and
in most cases is considered a bug.  There are two distinct type of looping
flow control structures in Python.

10.1: datatype List
-----------------------
In Python, one way to create a group of objects is called a List.

.. code:: pycon

    >>> list(1, 2, 3, 4)
    [1, 2, 3, 4]
    >>> # You can also create a list by using square brackets []
    ... ['a', 'tom', 1, 3.5]
    ['a', 'tom', 1, 3.5]
    >>> # as you can see, lists can be made up of different types
    ... 
    >>> # lists can be added together
    ... [1, 2, 3] + [4, 5]
    [1, 2, 3, 4, 5]
    >>> 


A list can be iterated over. Which means that there is a starting place and
you can move to the next item.  This idea of being able to iterate over items
in a group is so common that we refer to those kinds of data types as
iterables.

10.2: For loop
----------------
A For loop, iterates over a group of things until there are no more. The end
of the list is the end the loop.  Here is what a For loop looks like:

.. code:: pycon

    >>> for number in [1, 2, 3, 4, 5]:
    ...     print('number is %s' % number)
    ... 
    number is 1
    number is 2
    number is 3
    number is 4
    number is 5
    >>> 

**for** is beings the structure. Followed by an identifier that will be
 assigned to the item in the body of the loop.

**in** specifies that we are going to iterate over an iterable. And the
 statement is ended with a colon after the iterable.

The next line is indented and forms that start of the For loop code block.
Each line that is on the same indent is a part of that loop until a dedented
Python statement is encountered.

  **for** identifier **in** iterable**:**
      for-loop code block

  not_a_part_of_loop()



Section Y - Blocks and Scope
============================
Code execution and Variable visibility.

Y.1: Blocks
------------
A block is a piece of Python program text that is executed as a unit. The
following are blocks: a module, a function body, and a class definition.

Let's take a look at some code:

.. code:: python
   :number-lines:

    def my_function():
        """used as an example of code blocks."""
        print("in my_function")
        print("I print next.")

    my_function()

What do you expect to be the output?  **Run the code**

- Code is executed from Top to Bottom of the module.

  - Lines 1-4: a function object is created and the name my_function is assigned to it.

  - Line 6: my_func(), then is executed.

Now let's make a slight change to the code.

.. code:: python
   :number-lines:

    def my_function():
        """used as an example of code blocks."""
        print("in my_function")
    print("I print next.")

    my_function()

What will be the output of this code?  **Run the code**

- Code is executed from Top to Bottom of the module.

  - Lines 1-3: a function object is created and the name my_function is assigned to it.

    - my_function consists of 1 statement, ``print("in my_function")``

  - Line 4: ``print("I print next.")`` is executed.

  - Line 6: my_func(), is executed.

Remember that blocks are at the Module, Function body and Class level.  When
the ``print("I print next.")`` statement was dedented, it marked the end of
the function body block.  So, it then became a module level statement and is 
excuted in Top to Bottom order.

Y.2: Scope
-----------
A scope defines the visibility of a name within a block. If a local variable is
defined in a block, its scope includes that block. If the definition occurs in
a function block, the scope extends to any blocks contained within the defining
one, unless a contained block introduces a different binding for the name.

When a name is used in a code block, it is resolved using the nearest enclosing
scope. The set of all such scopes visible to a code block is called the block’s
environment.

If a name is bound in a block, it is a local variable of that block. If a name
is bound at the module level, it is a global variable.

When a name is not found at all, a NameError exception is raised. If the
current scope is a function scope, and the name refers to a local variable that
has not yet been bound to a value at the point where the name is used, an
UnboundLocalError exception is raised.


Quotes
======

"Everyone knows that debugging is twice as hard as writing a program in the
first place. So if you're as clever as you can be when you write it, how will
you ever debug it?"  -- Brian Kernighan (co-author of C language)


