=============================
py100 - Python for Beginners
=============================
Class is a survey workshop on programming with Python 3 for those new to
programming and new to Python.  It is designed to create interest in learning
more about Python and a base to build on for more in depth workshops.


Topics Covered
========================
See lecture notes for detailed information.

PythonAnywhere (PA) Account Setup and Teacher Designation
------------------------------------------------------------

- Into to PA web interface
- Account Settings
- Dashboard
- Consoles Tab
- Shared Consoles
- Files Tab
- File Editor

Intro to Programming
----------------------
- Machine Language to Python in 60 seconds
- What is a program Input -> Process -> Output

Python
-------
- The interpreter a/k/a Python Console
- ``print`` function  (basic) 
- function - generalized definition
- string
- ``string literal``
- type: ``str``
- Types - what are types?
- type: ``int``
- ``type`` function
- Strings - quoting single v. double quotes
- Strings - concatentation
- Comments
- Strings - interpolation with modulo (%)
- Variables
- Identifiers
- ``help`` function
- Functions - purpose, first class functions
- arguments
- docstrings
- type: ``bool``
- conditionals
- comparison operators
- ``if`` / ``elif`` / ``else``
- type: ``float``


Materials
========================


Labs Utilized
========================
- ``student`` file - very basic file creation and editing of a text file.
- helloworld - first python module.
- helloworld (refactored) to use interpolation for 'World'.
- hello_sam - a variation of helloworld but using a function to build the
  string to print out.
- organize_sam - compare two things and return their ordering.
- hogwarts - convert a numeric grade to a letter grade using a 7pt scale.

