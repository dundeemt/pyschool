"""A card game for Padawans.
You are dealt two cards from a deck containing 4 each of 1's, 2's, 3's, 4's and
a single 5 card.  The goal is to reach 9 by adding the values of the cards
in your hand, or be the closest to 9 without going over."""
import random


class Gambler(object):
    """Someone who gambles"""
    PLAYING = 'playing'
    STANDING = 'standing'
    BUSTED = 'busted'

    def __init__(self, name):
        self.name = name
        self.hand = []
        self.status = Gambler.PLAYING

    def take_card(self, card):
        """take the card dealt"""
        self.hand.append(card)
        if self.hand_value() == 9:
            self.status = Gambler.STANDING
        if self.hand_value() > 9:
            self.status = Gambler.BUSTED
        return "%s has %s and is %s" % (self.name, self.hand_value(),
                                        self.status)

    def hand_value(self):
        """return the value of the gambler's hand"""
        return sum(self.hand)

    def wants_card(self):
        """Return True if another card is wanted."""
        if self.status != Gambler.PLAYING:
            return False
        if len(self.hand) < 2:
            return True
        msg = "%s has %s cards: value %s, want a card (Y/n)"
        while True:
            answer = input(msg % (self.name, self.hand, sum(self.hand)))
            if answer.lower() == 'y':
                return True
            elif answer.lower() == 'n':
                self.status = Gambler.STANDING
                return False
            else:
                print("Don't understand %s" % answer)

    def busted(self):
        """Is player busted?"""
        return self.status == Gambler.BUSTED


class Dealer(Gambler):
    """Dealer has automatic rules."""
    def wants_card(self):
        """Hits on 5, stands on 6 or above."""
        if self.status != Gambler.PLAYING:
            return False
        if len(self.hand) < 2:
            return True
        # automatic hit on 5 or less, stand on anything better
        if self.hand_value() <= 5:
            return True
        else:
            self.status = Gambler.STANDING
            return False


def deal_a_card(deck):
    '''remove the top card from the deck(cards) and return it.'''
    return deck.pop(0)


def shuffle_deck():
    '''return a deck(list) of cards, in random order.'''
    # cards = [1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4, 5]
    cards = 4 * [1, 2, 3, 4] + [5]
    random.shuffle(cards)
    return cards


def game_on(players):
    """Return True if game is still on."""
    status = False
    for player in players:
        if player.status == Gambler.PLAYING:
            status = True
            break
    return status


def outcomes(players):
    """Return iterable of results of the game now that play has stopped."""
    for player in players:
        if player.name != 'Dealer':
            continue
        dealer = player
    for player in players:
        if player is not dealer:
            if dealer.busted():
                if player.busted():
                    yield '%s push' % player.name
                else:
                    yield '%s wins' % player.name
            else:
                if player.busted():
                    yield '%s lost' % player.name
                else:
                    if player.hand_value() > dealer.hand_value():
                        yield '%s won' % player.name
                    elif player.hand_value() == dealer.hand_value():
                        yield '%s push' % player.name
                    else:
                        yield '%s lost' % player.name


def main():
    """main game loop"""
    players = [Gambler('Jeff'), Dealer('Dealer')]
    print("New Game. Shuffling cards....")
    cards = shuffle_deck()
    while game_on(players):
        for player in players:
            if player.wants_card():
                print(player.take_card(deal_a_card(cards)))
            else:
                print("%s is %s with %s" % (player.name, player.status,
                                            player.hand_value()))
    # game is over, display results
    for outcome in outcomes(players):
        print(outcome)


if __name__ == "__main__":
    main()
