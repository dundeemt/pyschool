==============================
Jedi Nine
==============================
GOAL: Accumulate cards to reach a sum of 9 without going over 9.

Setup
------
- From a deck containing 4 each of 1's, 2's, 3's, 4's and a single 5 card.
- Cards are dealt in an alternating manner. (Player, Dealer, Player,
  Dealer, ...)

Play
-----
- After two cards are dealt to each player and dealer, each can decide if they
  want another card.
- If the player does not want a card, they are standing on their current points
  and the deal continues to the next player.
- If the player does want a card, they are dealt a card.
- If the new card pushes their points past 9, they are then 'busted' and out of
  that current game.
- The deal continues until all players are standing or busted.
- The dealer stands on 6 or better.

Outcome
--------
  - All players, play against the dealer.
  - If the player has the same points as the dealer, it is a push
  - If both the player and dealer are busted, it is a push
  - If the player is closer to 9 than the dealer, they win.
