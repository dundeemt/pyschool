lab: hogwarts
=============
Sam read the Harry Potter series of books and is considering attending Hogwart's School.  
Sam doesn't realize that it is a fictional school and wants to figure out what his letter grade would be based on Sam's current GPA.

synopsis
--------
Edit a function that has 1 arg, grade, and return the appropriate letter grade for a 7 point scale.

  - level: 0
  - requires: ["comparisons", "if", "functions", "parameters"]
  - inspires: ["modules",  "informal testing"]

