"""tests for hogwarts.py"""
import pytest       # pylint:disable=e0401

from hogwarts import convert_to_letter
from hogwarts import convert_to_letter_backwards


EDGE_CASES = [
    (102, 'A'),
    (93, 'A'),
    (92, 'B'),
    (86, 'B'),
    (85, 'C'),
    (79, 'C'),
    (78, 'D'),
    (72, 'D'),
    (71, 'F'),
    (0, 'F'),
    (-1, 'F'),
    (99.99, 'A')
]


@pytest.mark.parametrize("grade, letter", EDGE_CASES)
def test_convert_to_letter(grade, letter):
    """code to test original function, evaluating from A->F"""
    assert convert_to_letter(grade) == letter


@pytest.mark.parametrize("grade, letter", EDGE_CASES)
def test_convert_to_letter_bwards(grade, letter):
    """code to test student function, evaluating from F->A"""
    assert convert_to_letter_backwards(grade) == letter


@pytest.mark.parametrize("grade", list(range(-1, 102)))
def test_forward_to_back(grade):
    """compare grades -1 -> 101 from the two functions"""
    assert convert_to_letter(grade) == convert_to_letter_backwards(grade)
