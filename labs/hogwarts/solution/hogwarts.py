#!/usr/bin/env python3
"""hogwarts - functions to convert numeric grade to a letter grade."""


def convert_to_letter(grade):
    """Convert the numeric grade to a letter grade using Hogwart's 7 point
    scale."""
    if grade >= 93:
        letter = 'A'
    elif grade >= 86:
        letter = 'B'
    elif grade >= 79:
        letter = 'C'
    elif grade >= 72:
        letter = 'D'
    else:
        letter = 'F'
    return letter


def convert_to_letter_backwards(grade):
    """convert numeric grade to letter, testing from F->A"""
    if grade < 72:
        letter = 'F'
    elif grade < 79:
        letter = 'D'
    elif grade < 86:
        letter = 'C'
    elif grade < 93:
        letter = 'B'
    else:
        letter = 'A'
    return letter
