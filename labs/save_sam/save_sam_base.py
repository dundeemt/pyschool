#!/usr/bin/env python3
"""save_sam_base - a hangman like game"""

import os


class FlipBoard(object):
    """object to represent flipboard"""
    def __init__(self, word, mask='-'):
        self.board = [list(panel) for panel in zip(word, len(word) * mask)]
        self.match(' ')
        self.mask = mask

    def match(self, char):
        '''if matched, reveal letter(s) and return number of matches'''
        matched = 0
        for panel in self.board:
            if char.lower() == panel[0].lower():
                matched += 1
                panel[1] = panel[0]
        return matched

    @property
    def state(self):
        '''return the state of the flipboard'''
        return [panel[1] for panel in self.board]

    @property
    def solved(self):
        '''return True if the board has been completely revealed.'''
        return self.mask not in self.state


class SaveSamBase(object):

    """Base object to implement the game."""
    def __init__(self):
        self.sam = "    0\n  / Y \\ \n    ^\n   / \\ \n\n   Sam"
        self.incorrect = []
        self.flipboard = FlipBoard(self.random_word(), mask='#')

    def play(self):
        """start the game running"""
        while True:
            self.display_gameboard()

            if self.player_lost:
                print('Sam is Gone!')
                return False

            char = self.get_guess()
            if self.flipboard.match(char):
                if self.player_won:
                    self.display_gameboard()
                    print('You Saved Sam!')
                    return True
            else:
                self.incorrect.append(char)
                self.disappear_more()

    @property
    def player_lost(self):
        """If sam is gone, return True"""
        return not self.sam

    @property
    def player_won(self):
        """If we have found the word, return True"""
        return self.flipboard.solved

    def display_gameboard(self):
        """Display the board status of the game."""
        os.system('cls' if os.name == 'nt' else 'clear')
        print(self.sam)
        print((5-self.sam.count('\n')) * '\n')
        print(' '.join(self.flipboard.state))
        print()
        print("INCORRECT: ", self.incorrect)

    def get_guess(self):
        """get a character of input from the user."""
        while True:
            char = input('Enter a letter:')
            char = char.strip()
            if len(char) == 1 and char.isalpha():
                if char in self.incorrect:
                    print('%s Already guessed. Please try again.' % char)
                else:
                    return char
            else:
                print('Not recognized. Please enter a single alpha character.')

    def disappear_more(self):
        '''make another piece of Sam dissappear.'''
        self.sam = self.sam[:-1].rstrip()

    def random_word(self):
        """return a randomly selected word."""
        raise NotImplementedError


def main():
    """start the game"""
    save_sam = SaveSamBase()
    save_sam.play()


if __name__ == "__main__":
    main()
