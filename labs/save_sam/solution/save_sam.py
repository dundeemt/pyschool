#!/usr/bin/env python3
"""make a version of the game with words/phrases of your own choosing."""

from random import choice

from save_sam_base import SaveSamBase


class SaveSam(SaveSamBase):
    """Subclass SaveSamBase and put in our own words."""

    def random_word(self):
        """return a random word for the game to use."""
        return choice(['password', 'fido'])


def main():
    """start the game"""
    save_sam = SaveSam()
    save_sam.play()


if __name__ == "__main__":
    main()
