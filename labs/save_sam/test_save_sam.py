"""test save_sam"""

from save_sam import SaveSam


def test_random_word_more_than_one():
    '''test that Subclass SaveSam.random_word returns more than just 1 random
    word.'''
    save_sam = SaveSam()
    assert len(set([save_sam.random_word() for i in range(10)])) > 1
