save_sam
========

**save_sam.py**  - Save Sam from disappearing by guessing the secret word before you luck runs out and Sam evaporates.
Sam and you are in the basement of YoYoDyne, Inc. and Sam is locked in the malfunctioning matter transporter chamber. They system is scanning
him now and when it is complete, Sam will be dissasmbled on a molecular level and put back together who knows where and incorrectly.  Someone
has put a password on the control interface and you need to guess the password to stop the scan and save Sams life.

::

    0
  / Y \
    ^
   / \
   Sam


Once you have saved Sam, open ``save_sam.py`` and write a better ``random_word``
method.  Since the average person can memorize a list of 7 items, have
random_word randomly return 1 of 21 different words to make the locking
mechanism harder to guess.
