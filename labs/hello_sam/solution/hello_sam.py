#!/usr/bin/env python3
"""program hello_sam.py"""


def hello(name='Sam'):
    '''given a name, say Hello!'''
    # could also be: 'Hello, ' + name + '.'
    # if you haven't touch string interpolation yet.
    return 'Hello, %s!' % name

#                             ouputs ...
print(hello())              # Hello, Sam!
print(hello('World'))       # Hello, World!
print(hello(name='Sara'))   # Hello, Sara!
