#!/usr/bin/env python3
"""program hello_sam.py"""


def hello(name='Sam'):
    '''given a name, say Hello'''
    # replace the following pass statement with the code needed to return
    # a string formatted to say Hello.  The parameter, name, has a default
    # value of 'Sam' when no parameter is specified. Otherwise it will be
    # the string to use.
    pass


#                           Should output ...
print(hello('World'))       # Hello, World!
print(hello(name='Sara'))   # Hello, Sara!
print(hello())              # Hello, Sam!
