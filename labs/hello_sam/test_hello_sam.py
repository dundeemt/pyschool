"""Tests for hello_sam.py"""

from hello_sam import hello


def test_hello():
    """call function with no parameter"""
    assert hello() == "Hello, Sam!"


def test_hello_world():
    """call function passing 'World'"""
    assert hello('World') == 'Hello, World!'
