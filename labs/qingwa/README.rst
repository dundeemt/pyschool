================
Qingwa
================
pronounced Ching-Wa 
A dice game played in the rim and favored by Jane.
(Variation on craps)

- 2 6-sided dice
- rolled together
- 1st roll, a total of 2, 3 or 12 is a Qingwa and the shooter loses.
- 1st roll, a total of 7 or 11 and the shooter wins.
- 1st roll is a 4, 5, 6, 8, 9 or 10 that is the player's point.
- 2nd and remaining rolls, player tries to make their point before they roll a
  7 (Qingwa)
