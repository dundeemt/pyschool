#!/usr/bin/env python3
"""secret_sam.py - Sam's younger brother is always nosing around. Help Sam
keep his secrets, secret by writing a Ceaser Cipher encoder/decoder.

A B (C) D E F G H I J K L M (N) O P Q R S T U V W X Y Z
     ^ --------- 11 -------- ^

http://practicalcryptography.com/ciphers/caesar-cipher/
"""


class Ceasar(object):
    """Implement a Ceasar cipher encoder/decoder."""

    def __init__(self, shift, wheel=None):
        self.shift = shift
        if wheel is None:
            self.wheel = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
        else:
            self.wheel = wheel

    def encode(self, plaintext):
        """encode plaintext using shift/wheel given."""
        cipher = ''
        for char in plaintext:
            ndx = self.wheel.index(char)
            cipher += self.wheel[(ndx+self.shift) % len(self.wheel)]
        return cipher

    def decode(self, cipher):
        """decode cipher using shift/wheel given."""
        plaintext = ''
        for char in cipher:
            ndx = self.wheel.index(char)
            plaintext += self.wheel[(ndx - self.shift) % len(self.wheel)]
        return plaintext


def test():
    """test the code"""
    ceasar = Ceasar(shift=11)
    assert ceasar.encode('C') == 'N'
    assert ceasar.decode('N') == 'C'
    msg = 'BESURETODRINKYOUROVALTINE'
    assert ceasar.decode(ceasar.encode(msg)) == msg


if __name__ == '__main__':
    test()
