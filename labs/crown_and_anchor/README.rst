=====================
Crown and Anchor
=====================
https://en.wikipedia.org/wiki/Crown_and_Anchor

Variation to keep it simple:

- 3 6-sided dice are used
- player starts with $100
- Each bet is $10
- Player enters a string of chars, "123" would be $10 each on
  1, 2 and 3 costing $30 total.  "111" would bet $30 on 1.
- Player is not be allowed to wager more than they have available.
- Dice are rolled.
  - Player loses wager on any not shown on dice.
  - if the roll is 1-2-3 then wagers on 1, 2 and 3 pay out at 1 to 1.
  - if the roll is 1-1-3 then wagers on 1 pay out at 2 to 1, and 3 is 1 to 1.

