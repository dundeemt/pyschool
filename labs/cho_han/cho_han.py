#! /usr/bin/env python3
"""Cho Han - a dice game.
2 6-sided dice are placed in a cup, shaken and turned upside-down.
Players then bet whether the sum of the die are Cho (even) or Han (odd)."""

import random

CHO = 'Cho'
HAN = 'Han'
QUIT = 'Quit'


def roll_die(num_sides=6):
    """return the results of a num_side dice roll."""
    return random.randrange(1, num_sides+1)


def shake_and_flip(num_dice=2, num_sides=6):
    """return a list of random dice rolls"""
    return [roll_die(num_sides) for dice in range(num_dice)]


def cho_or_han(cup):
    """Return Ho if even, else Chan"""
    if sum(cup) % 2 == 0:
        return CHO
    return HAN


def get_bet():
    """Return the players bet of (C)ho, (H)an or (Q)uit"""
    bet = ""
    while not bet:
        bet = input("(C)ho, (H)an or (Q)uit?")
        if bet in ['c', 'C']:
            bet = CHO
        elif bet in ['h', 'H']:
            bet = HAN
        elif bet in ['q', 'Q']:
            bet = QUIT
        else:
            print("%s not understood." % bet)
            bet = ""
    return bet


def game_on(bet):
    """Return True if player is not quitting."""
    return bet != QUIT


def main():
    """main game"""
    bet = ""
    while game_on(bet):
        cup = shake_and_flip()
        bet = get_bet()
        if game_on(bet):
            result = cho_or_han(cup)
            if bet == result:
                print("You win - %s %s" % (result, cup))
            else:
                print("You Loose - %s %s" % (result, cup))

    print("Thanks for playing.")

if __name__ == "__main__":
    main()
