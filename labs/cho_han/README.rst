=============
Cho Han
=============
Chō-Han Bakuchi or simply Chō-Han (丁半?)) is a traditional Japanese gambling
game using dice.

The game uses two standard six-sided dice, which are shaken in a bamboo cup or
bowl by a dealer. The cup is then overturned onto the floor. Players then
place their wagers on whether the sum total of numbers showing on the two
dice will be "Chō" (even) or "Han" (odd). The dealer then removes the cup,
displaying the dice. The winners collect their money.

Depending on the situation, the dealer will sometimes act as the house,
collecting all losing bets. But more often, the players will bet against each
other (this requires an equal number of players betting on odd and even) and
the house will collect a set percentage of winning bets.

The game was a mainstay of the bakuto, itinerant gamblers in old Japan, and
is still played by the modern yakuza. In a traditional Chou-Han setting,
players sit on a tatami floor. The dealer sits in the formal seiza position
and is often shirtless (to prevent accusations of cheating), exposing his
elaborate tattoos.
