"""test sorting_sam.py"""

from organize_sam import organize


def test_numbers():
    """test comparing numbers"""
    assert organize(5.0, 4) == "4 comes before 5.0"
    assert organize(2, 3) == "2 comes before 3"
    assert organize(1, 1) == "1 and 1 are equal"


def test_strings():
    """test comparing strings"""
    assert organize('a', 'b') == "a comes before b"
    assert organize('Jim', 'Jeff') == "Jeff comes before Jim"
    assert organize('Zero', 'Zero') == "Zero and Zero are equal"
