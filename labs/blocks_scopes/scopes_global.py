#!/usr/bin/env python3
"""Demo of global variable scope"""

# PI is a global variable, specifically a Constant Variable.  By convention,
# such constants are in all UPPER case.
PI = 3.14159267


def circle_area(radius):
    """return the area of a circle with a given radius."""
    # radius squared = radius * radius = radius ** 2
    # ** is the exponentiation operator
    return 2 * PI * radius * radius


def circle_circumference(radius):
    """return the circumference of circle with a given radius."""
    return 2 * PI * radius


def main():
    """print out the area and circumference of some circles."""
    print("Using PI with an approximate value of %s" % PI)
    print()
    radius = 1.5
    print("The area of a circle radius %s is %s" % (radius,
                                                    circle_area(radius)))
    print("Its circumference is %s" % (circle_circumference(radius)))


main()
