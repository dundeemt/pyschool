#! /usr/bin/evn python3
"""demonstration of scope"""


def demo():
    """demo scope of a variable defined in a function block"""
    # my_var is local to demo and only visisble to code in this block.
    my_var = 'Yes'
    return "Can I see 'my_var':" + my_var


def main():
    """run the demo"""
    print(demo())
    # The following code will fail because my_var is not defined in it's scope
    # nor in it's ancestor's scope, the module.
    print("Can I see 'my_var':" + my_var)


main()
