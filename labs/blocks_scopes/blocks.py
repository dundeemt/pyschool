#! /usr/bin/evn python3
"""used to demonstrate blocks and execution order."""

# <- code that starts at this indentation level is in the module block.


def my_function():
    # <- code defined at this level is in a function level block
    """used as an example of code blocks"""
    print("in my_function")
    print("I print next.")


my_function()
