#!/usr/bin/env python3
"""vowels.py - upper case all the vowels, lower case all consonants."""
from __future__ import print_function


def upper_vowels(chars):
    """upper case vowels, lower case consonants and return result."""
    buf = ''
    for char in chars:
        if char.lower() in "aeiou":
            buf += char.upper()
        else:
            buf += char.lower()
    return buf


def test():
    """test the function."""
    buf = 'Be sure to drink your Olvaltine!'
    result = 'bE sUrE tO drInk yOUr OlvAltInE!'
    assert upper_vowels(buf) == result

if __name__ == "__main__":
    test()
