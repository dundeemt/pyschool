"""Test helloworld."""


# pylint:disable=w0612
def test_helloworld(capsys):
    """test that it outputs 'Hello World!'"""
    import helloworld       # NOQA
    out, err = capsys.readouterr()
    assert out == "Hello World!\n"
    assert err == ""


def test_helloworld_docstring():
    """test that a docstring has been written."""
    import helloworld
    dstring = helloworld.__doc__
    assert dstring
    assert dstring != "docstring goes here"
