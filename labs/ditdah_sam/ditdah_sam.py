#! /usr/bin/evn python3
"""ditdah_sam.py - a morse code encoder/decoder
http://morsecode.scphillips.com/morse2.html"""
from __future__ import print_function
try:
    input = raw_input
except NameError:
    pass


class Morse(object):
    """A Morse Code encoder/decoder"""
    CODE = {'A': ".-", 'B': "-...", 'C': "-.-.", 'D': "-..", 'E': ".",
            'F': "..-.", 'G': "--.", 'H': "....", 'I': "..", 'J': ".---",
            'K': "-.-", 'L': ".-..", 'M': "--", 'N': "-.", 'O': "---",
            'P': ".--.", 'Q': "--.-", 'R': ".-.", 'S': "...", 'T': "-",
            'U': "..-", 'V': "...-", 'W': ".--", 'X': "-..-", 'Y': "-.--",
            'Z': "--..", '0': "-----", '1': ".----", '2': "..---",
            '3': "...--", '4': "....-", '5': ".....", '6': "-....",
            '7': "--...", '8': "---..", '9': "----.", '.': ".-.-.-",
            ',': "--..--", ':': "---...", '?': "..--..", "'": ".----.",
            '-': "-....-", '/': "-..-.", '(': "-.--.-", '"': ".-..-.",
            '@': ".--.-.", '=': "-...-"}

    def encode(self, chars):
        '''encode a character buffer in to Morse'''
        encoded = ''
        for char in chars:
            if char == ' ':
                encoded += '  '
            else:
                encoded += '%s ' % Morse.CODE[char]
        return encoded

    def decode(self, mbuf):
        """decode a morse code buffer to characters."""
        decoded = ''
        code = dict((value, key) for key, value in Morse.CODE.items())
        for word in mbuf.split('  '):
            for char in word.strip().split(' '):
                decoded += code[char]
            decoded += ' '
        return decoded.rstrip()


def test():
    """Test your code"""
    morse = Morse()
    msg = 'BE SURE TO DRINK YOUR OVALTINE'
    assert morse.decode(morse.encode(msg)) == msg
    msg = 'HTTP://WWW.OMAHAPYTHON.ORG/'
    assert morse.decode(morse.encode(msg)) == msg
    user_input = input('Enter your message or code:')
    try:
        print(morse.decode(user_input))
    except KeyError:
        print(morse.encode(user_input))


if __name__ == "__main__":
    test()
