jedi_academy
============
Luke is busy restarting the Jedi Academy.  However, he is unsure what grade
point system to use for padawans.  Luke needs you to write a function
jedi_grade that takes the padawan's numeric grade and the grade scale to use
and returns a letter grade for the numeric grade based on the scale.

.. code:: pycon

    >>> jedi_grade(grade=89, scale=10)
    B

grades can be 0 to 100+ and the scale can be 1 to 100.

May the force be with you.
