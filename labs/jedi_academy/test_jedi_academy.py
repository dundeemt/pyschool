"""test the jedi_grade function"""
import pytest       # pylint:disable=e0401

from jedi_academy import jedi_grade


@pytest.mark.parametrize("grade, letter", [
    (100, 'A'),
    (90, 'A'),
    (89, 'B'),
    (80, 'B'),
    (79, 'C'),
    (70, 'C'),
    (69, 'D'),
    (60, 'D'),
    (59, 'F'),
    (50, 'F'),
    (0, 'F'),
])
def test_scale10(grade, letter):
    """test grading on a 10 pt scale."""
    assert jedi_grade(grade, 10) == letter


@pytest.mark.parametrize("grade, letter", [
    (100, 'A'),
    (91, 'A'),
    (90, 'B'),
    (82, 'B'),
    (81, 'C'),
    (73, 'C'),
    (72, 'D'),
    (64, 'D'),
    (63, 'F'),
    (50, 'F'),
    (0, 'F'),
])
def test_scale9(grade, letter):
    """test grading on a 9 pt scale."""
    assert jedi_grade(grade, 9) == letter


@pytest.mark.parametrize("grade, letter", list(
    zip([100, 95, 94, 90, 89, 85, 84, 80, 50, 0], "AABBCCDDFFF")))
def test_scale5(grade, letter):
    """test grading on a 5 pt scale."""
    assert jedi_grade(grade, 5) == letter


@pytest.mark.parametrize("grade, letter", list(
    zip([100, 99, 98, 97, 96, 95, 94, 50, 0], "AABCDFFF")))
def test_scale1(grade, letter):
    """test grading on a 1 pt scale."""
    assert jedi_grade(grade, 1) == letter
