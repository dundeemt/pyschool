"""Letter grade calculator for Luke's Jedi Academy."""


def jedi_grade(grade, scale):
    """return the appropriate letter for the numeric grade for a given scaling
    system."""
    if grade >= 100 - scale:
        return 'A'
    elif grade >= 100 - 2 * scale:
        return 'B'
    elif grade >= 100 - 3 * scale:
        return 'C'
    elif grade >= 100 - 4 * scale:
        return 'D'
    else:
        return 'F'
