#!/usr/bin/env python3
"""rewrite of organize function for training Sam's neural nets."""


def organize(this, that):
    """compare this and that then return a string stating which one comes
    before the other OR if they are equal, say so."""

    # output format strings
    comes_before = "%s comes before %s"
    are_same = "%s and %s are equal"

    # write code below to return proper message using output format strings
    if this < that:
        result = comes_before % (this, that)
    elif this > that:
        result = comes_before % (that, this)
    else:
        result = are_same % (this, that)
    return result


def test():
    """run the organize function over some simple tests."""
    tests = [
        (2, 1),
        (5.0, 5),
        ('banana', 'apple'),
        ('Tina', 'Steve')
    ]
    for things in tests:
        print('Comparing %s and %s -> ' % things,
              organize(things[0], things[1]))

if __name__ == "__main__":
    test()
