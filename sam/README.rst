Student Asset Manager 
=====================

::

        Usage: sam [OPTIONS] COMMAND [ARGS]...

          Student Asset Manager - sam

          Sam is a utility to assist managing student assets for workshops and labs
          in a pythonanywhere education environment.

        Options:
          --help  Show this message and exit.

        Commands:
          handout
          prep     prepare a workshop environment for the...
          roster   display current student roster.

Setup
------
Since sam is specific to pyschool + PA, currently it is not on the cheeseshop,
you just install it via your local repos.

- cd into the directory, ``pyschool/sam``
- ``pip install -e .``

