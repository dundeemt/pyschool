from setuptools import setup

setup(
    name='sam',
    version='0.0.1',
    py_modules=['sam'],
    install_requires=[
        'Click',
    ],
    entry_points='''
        [console_scripts]
        sam=sam:cli
    ''',
)