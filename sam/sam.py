"""sam - student assist module
a utility to disperse and test labwork, handle student roster duties, and
collect evaluations."""

import glob
import os

import click

PYSCHOOL = 'pyschool'

class Student(object):
    """representation of a student"""
    def __init__(self, path):
        self.path = path
        self._name = None
        self._make_school()

    @property
    def name(self):
        if self._name is None:
            try:
                fname = os.path.join(self.path, PYSCHOOL, 'student')
                with open(fname, 'r') as fhndl:
                    temp_name = fhndl.readline().strip()
            except FileNotFoundError:
                return 'N/A'
            if not temp_name:
                return 'N/A'
            self._name = temp_name

        return self._name

    def _make_school(self):
        '''create the primary directory structure'''
        school_dir = os.path.join(self.path, PYSCHOOL)
        try:
            os.makedirs(school_dir)
        except OSError:
            if not os.path.isdir(school_dir):
                raise
        student_file = os.path.join(school_dir, 'student')
        touch(student_file)


def students():
    """an iterator over the students that have you setup as their teacher."""
    teacher = os.environ['HOME']
    for path in glob.glob('/home/*'):
        if path != teacher:
            yield Student(path=path)


def touch(fname):
    try:
        os.utime(fname, None)
    except:
        open(fname, 'a').close()


@click.group()
def cli():
    """Student Asset Manager - sam

    \b
    Sam is a utility to assist managing student assets for workshops and labs
    in a pythonanywhere education environment."""
    pass

@cli.command()
def roster():
    """display current student roster."""
    click.echo('Displaying the Class Roster')
    for student in students():
        click.echo('%s\t%s' % (student.name.ljust(20), student.path))

@cli.command()
def handout():
    click.echo('Copying the lab work to the students')

@cli.command()
def prep():
    """prepare a workshop environment for the current roster.

    \b
    A virtualenv for the named workshop is created under the student's pyschool
    folder.  any modules listed in the workshop.json config are installed there
    also along with a Goals file for the student."""
    click.echo('prepare a workshop environment for the students')
